Simple 2FA/MFA SSH Demo
---

As a POC to understand what a multi-factor authentication over SSH feels like, I cut a very simplistic Dockerfile that:

- sets up a Ubuntu-18.04 machine
- runs sshd
- allows password authentication for the root user
- specifies "ChallengeResponseAuthentication yes"
- installs the google-authenticator PAM library

The `build-and-run.sh` bash script, as the name suggests, builds the docker image based on the Dockerfile and names it test_ssh, boots the container and runs `google-authenticator` which provides console-interactive interface to configure Google Authenticator for time-based token two-factor authentication (which should present a QR code to you so you can set up your Google Authenticator app), commits the config changes and restarts the container (which is named `ssh-otp`, and the docker run command maps port `2200` on the host to `22` on the Docker container)

_google-authenticator responses:_
`y`
`y`
`n`
`y`
`n`

When you're ready to try it out, `ssh -p 2200 root@localhost`, the password for root is `testpassword`

===
*all the usual advice about running a root-enabled ssh daemon on your machine should apply. USE WITH GOOD COMMON SENSE*
===


Referenced Links
=== 

* https://hackertarget.com/ssh-two-factor-google-authenticator/
* https://docs.docker.com/engine/examples/running_ssh_service/

Reading to do
===
* https://blog.tankywoo.com/linux/2013/09/14/ssh-passwordauthentication-vs-challengeresponseauthentication.html
* https://ocramius.github.io/blog/yubikey-for-ssh-gpg-git-and-local-login/