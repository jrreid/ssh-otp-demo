#!/bin/bash
docker build -t test_ssh .

docker run -d \
--name ssh-otp \
-p 2200:22 \
test_ssh

docker exec -it ssh-otp google-authenticator

docker commit ssh-otp test_ssh
docker restart ssh-otp
